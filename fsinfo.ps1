$dir=$args[0]
[string]$dirRoot=$dir[0] + $dir[1]
$disk=Get-CimInstance Win32_LogicalDisk | Where-Object {$_.DeviceID -eq "$dirRoot"}
[int]$per=($disk.FreeSpace/$disk.Size)*100
$noOfFiles=(Get-ChildItem $dir -Recurse -File).count
$avgFileSize=Get-ChildItem $dir -Recurse -File | Measure-Object -Property Length -average | select-object -Expand average
$largestFileSize=(Get-ChildItem $dir -Recurse -File | sort Length -descendin | select-object -first 1).Length
$path=(Get-ChildItem $dir -Recurse -File | sort Length -descendin | select-object -first 1 | Select-Object Fullname).Fullname
write-output "Partisjonen er $per % full"
write-output "det finnes $noOfFiles filer i dette directoriet"
write-output "gjennomsnittlig filstoerrelse er $avgFileSize B"
write-output "Largest file is $path and it's $largestFileSize B big "