$dato=(Get-Date).ToString("yyyyMMdd")
$time=(Get-Date).ToString("hhmmss")
for($i=1; $i-le$args.Count; $i++) {
$ID=$args[$i-1]
$file="$ID-$dato-$time.meminfo"
new-item $file | out-null
$workingSet = (Get-Process -Id $ID | Select-Object WS | Format-Table WS -HideTableHeaders | Out-String).Trim()
$memory = (Get-Process -Id $ID | Select-Object vm | Format-Table VM -HideTableHeaders | Out-String).Trim()
$workingSet=$workingSet/1024
[int]$memory=$memory/1048576
add-content $file "******** Minne info om prosess med PID $ID  ********"
add-content $file "'nTotal bruk av virtuelt minne: $memory MB"
add-content $file "'nStoerrelse paa Working Set: $workingSet KB "

}


