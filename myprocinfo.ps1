write-output "Velg et tall"
write-output "1 - Hvem er jeg og hva er navnet paa dette scriptet?"
write-output "2 - Hvor lenge er det siden siste boot?"
write-output "3 - Hvor mange prosesser og traader finnes?"
write-output "4 - Hvor mange context switch'er fant sted siste sekund?"
write-output "5 - Hvor stor andel av CPU-tiden ble benyttet i kernelmode og i usermode siste sekund?"
write-output "6 - Hvor mange interrupts fant sted siste sekund?"
write-output "9 - Avslutt dette scriptet"
while($ans -ne 9){
$ans = Read-Host
switch ($ans) {
	1 {
	$whoami=whoami
	Write-Output "Dette er $whoami og navnet paa dette scriptet er myprocinfo.ps1"
	}
	2 {
		$uptime=(Get-CimInstance Win32_PerfFormattedData_PerfOS_System).SystemUpTime
		write-output "Det er $uptime sekunder siden sist boot"
	}
	3 {
		$threads=(Get-CimInstance Win32_Thread).count
		$processes=(get-process).count
		write-output "Det er $processes prosesser og $threads traader "
		}
	4 {
		$switcher=(Get-CimInstance Win32_PerfFormattedData_PerfOS_System).ContextSwitchesPerSec
		write-output "Antall switcher sist sekund $switcher"
	}
	5 {
		$kernel=((Get-Counter -Counter "\processor(_total)\% privileged time" -SampleInterval 1).CounterSamples | Format-Table CookedValue -HideTableHeader | Out-String).Trim()
		$user=((Get-Counter -Counter "\processor(_total)\% user time" -SampleInterval 1).CounterSamples | Format-Table CookedValue -HideTableHeader | Out-String).Trim()
		write-output "$kernel % ble brukt i kernelmode og $user % ble brukt i usermode"
	}
	6 {
		$interrupts=((Get-Counter -Counter "\processor(_total)\interrupts/sec").CounterSamples | Format-Table CookedValue -HideTableHeader | Out-String).Trim()
		write-output "interrupts siste sekund: $interrupts"
	}
	9 {}
	default {Write-Output "not valid"}
	}}
